FROM python:3.8-buster

# metadata
LABEL version="0.1"

# sets the working directory
WORKDIR /cvapp

# setup needed items and pull code
RUN apt-get update && apt-get install git -y
RUN git clone https://gitlab.com/crvallance/hello.git
WORKDIR /cvapp/hello
RUN pip3 install -r requirements.txt

# run the app
EXPOSE 5000
CMD python3 yep.py
